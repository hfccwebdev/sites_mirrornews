<?php

/**
 * @file
 * Views handlers for mysite.module.
 */

/**
 * Implements hook_views_data_alter().
 */
function mysite_views_data_alter(&$data) {
  // Add a dummy field on field_publication_date that provides
  // a lookup for the college name on that date.
  $data['field_data_field_publication_date']['field_college_name'] = array(
    'group' => t('Content'),
    'title' => t('College Name'),
    'help' => t('Return the college name for the given publication date.'),
    'real field' => 'field_publication_date_value',
    'field' => array(
      'handler' => 'mysite_handler_field_college_name',
      'click sortable' => FALSE,
    ),
  );
}

/**
 * Implements hook_field_views_data_alter().
 */
function mysite_field_views_data_alter(&$result, $field, $module) {
  if ($field['field_name'] == 'field_keywords') {
    $result['field_data_field_keywords']['field_keywords']['field']['handler'] = 'mysite_handler_field_keywords';
  }
}

/**
 * Return the college name for a given publication date.
 *
 * @ingroup views_field_handlers
 */
class mysite_handler_field_college_name extends views_handler_field {

  /**
   * Render the field.
   *
   * @todo Add date formatters. This just shows raw serial date.
   */
  public function render($values) {
    $value = $this->get_value($values);
    if (!empty($value)) {
      if ($value <= -757364400) {
        drupal_set_message('Invalid publication date @date found.', array('@date' => format_date($value)), 'error');
        return t('Cannot display college name.');
      }
      elseif ($value <= -568062000) {
        return t('Dearborn Junior College');
      }
      elseif ($value <= 1400472000) {
        return t('Henry Ford Community College');
      }
      else {
        return t('Henry Ford College');
      }
    }
  }

}

/**
 * Filter field_keywords taxonomy terms by field_subject_categories.
 */
class mysite_handler_field_keywords extends views_handler_field_field {

  /**
   * {@inheritdoc}
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['subject_to_filter'] = array('default' => 0);
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function options_form(&$form, &$form_state) {

    $result = db_query("SELECT tid,name FROM taxonomy_term_data WHERE vid=8 ORDER BY weight")->fetchAll();
    $options = array('' => '- Select -');
    foreach ($result as $term) {
      $options[$term->tid] = check_plain($term->name);
    }

    $form['subject_to_filter'] = array(
      '#type' => 'select',
      '#title' => t('Subject category'),
      '#options' => $options,
      '#default_value' => $this->options['subject_to_filter'],
      '#description' => t('Select subject filter.'),
      '#required' => FALSE,
    );
    parent::options_form($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function get_items($values) {
    $items = $values->{'field_' . $this->options['id']};
    if (empty($this->options['subject_to_filter'])) {
      return $items;
    }
    else {
      $subject = $this->options['subject_to_filter'];
      $result = array();

      foreach ($items as $item) {
        $term = $item['raw']['taxonomy_term'];
        if (!empty($term->field_subject_categories[LANGUAGE_NONE]) && $term->field_subject_categories[LANGUAGE_NONE][0]['tid'] == $subject) {
          $result[] = $item;
        }
      }
      return $result;
    }
  }

}
