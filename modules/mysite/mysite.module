<?php

/**
 * @file
 * Site-specific code.
 */

/**
 * Implements hook_menu().
 */
function mysite_menu() {
  return [
    'api/1.0/file/index' => [
      'title' => 'File API Index',
      'page callback' => 'MysiteApiController::fileIndex',
      'access arguments' => ['access content'],
      'type' => MENU_CALLBACK,
    ],
    'api/1.0/file/%file' => [
      'title' => 'File',
      'page callback' => 'MysiteApiController::file',
      'page arguments' => [3],
      'access arguments' => ['access content'],
      'type' => MENU_CALLBACK,
    ],
    'api/1.0/%/index' => [
      'title' => 'Content API Index',
      'page callback' => 'MysiteApiController::index',
      'page arguments' => [2],
      'access arguments' => ['access content'],
      'type' => MENU_CALLBACK,
    ],
    'api/1.0/node/%node' => [
      'title callback' => 'node_page_title',
      'title arguments' => [3],
      'page callback' => 'MysiteApiController::content',
      'page arguments' => [3],
      'access arguments' => ['access content'],
      'type' => MENU_CALLBACK,
    ],
    'api/1.0/tags' => [
      'title' => 'Tags API Index',
      'page callback' => 'MysiteApiController::tags',
      'access arguments' => ['access content'],
      'type' => MENU_CALLBACK,
    ],
    'api/1.0/users' => [
      'title' => 'User API Index',
      'page callback' => 'MysiteApiController::users',
      'access arguments' => ['access content'],
      'type' => MENU_CALLBACK,
    ],
  ];
}

/**
 * Implements hook_menu_breadcrumb_alter().
 */
function mysite_menu_breadcrumb_alter(&$active_trail, $item) {
  if (!drupal_is_front_page()) {
    // Quick fix. If breadcrumb will only include "Home" then hide it.
    $count = count($active_trail);
    if ($count <= 2) {
      $active_trail = array();
    }
  }
}

/**
 * Implements hook_form_alter().
 */
function mysite_form_alter(&$form, &$form_state, $form_id) {
  // This seems to be necessary or hook_form_BASE_FORM_ID_alter() won't execute.
}

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 */
function mysite_form_node_form_alter(&$form, &$form_state, $form_id) {
  switch ($form_id) {
    case 'article_node_form':
      $form['options']['#access'] = 1;
      $form['options']['status']['#access'] = TRUE;
      $form['options']['promote']['#access'] = TRUE;
      if (empty($form['options']['sticky']['#default_value'])) {
        $form['options']['sticky']['#disabled'] = TRUE;
      }
      break;

    case 'issue_node_form':
      $form['options']['#access'] = 1;
      $form['options']['status']['#access'] = TRUE;
      $form['options']['promote']['#disabled'] = FALSE;
      if (empty($form['options']['sticky']['#default_value'])) {
        $form['options']['sticky']['#disabled'] = TRUE;
      }
      break;
  }

  $form['field_weight']['#access'] = FALSE;
  $form['field_header_weight']['#access'] = FALSE;
  $form['field_collage_weight']['#access'] = FALSE;
  $form['field_radioactivity']['#access'] = FALSE;

  if (isset($form['revision_information'])) {
    // Hide revision fields on node add form.
    // @see https://drupal.org/node/2127375
    // @todo Should be removed in D8 #308820.
    if (!isset($form['#node']->nid)) {
      $form['revision_information']['#access'] = FALSE;
    }
  }

  $form['#after_build'][] = 'mysite_form_node_form_after_build';

}

/**
 * Applies additional form alters after build.
 */
function mysite_form_node_form_after_build($form, &$form_state) {
  // dpm($form);
  $form['author']['#weight'] = 99;
  $form['custom_breadcrumbs']['#access'] = FALSE;
  return $form;
}

/**
 * Implements hook_block_info().
 */
function mysite_block_info() {
  return array(
    'mysite_hfc_logo' => array(
      'info' => t('mysite: HFC Logo Block'),
      'cache' => DRUPAL_CACHE_GLOBAL,
    ),
  );
}

/**
 * Implements hook_block_view().
 */
function mysite_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    case 'mysite_hfc_logo':
      $block['content'] = mysite_hfc_logo();
      break;
  }
  return $block;
}

/**
 * Implements hook_views_api().
 */
function mysite_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'mysite') . '/views',
  );
}

/**
 * Implements hook_entity_info_alter().
 */
function mysite_entity_info_alter(&$entity_info) {
  $entity_info['node']['view modes']['collage'] = array(
    'label' => t('Front page collage'),
    'custom settings' => TRUE,
  );
}

/**
 * Implements HOOK_preprocess_node().
 */
function mysite_preprocess_node(&$vars) {
  if ($vars['view_mode'] == 'collage') {
    $vars['theme_hook_suggestions'][] = 'node__' . $vars['node']->type . '__collage';
  }
}

/**
 * Content for HFC Logo block.
 */
function mysite_hfc_logo() {
  $image = theme('image', array(
    'path' => drupal_get_path('module', 'mysite') . '/hfc-futuredriven-black-large-transparent.png',
    'alt' => t('Henry Ford College'),
  ));
  return l($image, 'https://www.hfcc.edu/', array('html' => TRUE));
}
