<?php

/**
 * @file
 * Default theme implementation to display an HFCC News Article.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $news_url: Direct URL of the current event.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions.
 * - $page: Flag for the full page state.
 *
 * @see template_preprocess()
 * @see template_preprocess_hfccbane_news()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>
<article id="news-<?php print $news['nid']; ?>">
  <?php print render($content); ?>
</article>
