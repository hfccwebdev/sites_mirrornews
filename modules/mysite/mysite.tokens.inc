<?php

/**
 * @file
 * Token callbacks for the mysite module.
 */

/**
 * Implements hook_token_info().
 */
function mysite_token_info() {
  $info = array();

  // Add any new tokens.
  $info['tokens']['node'] = array(
    'field_issue_title' => array(
      'name' => t('Issue Title'),
      'description' => t('A formatted version of multiple issue fields for the issue title.'),
    ),
    'field_issue_path' => array(
      'name' => t('Issue Path'),
      'description' => t('A version of field_publication_date suitable for pathauto use.'),
    ),
    'field_issue_publication_path' => array(
      'name' => t('Issue Publication Path'),
      'description' => t('A version of field_publication_title suitable for pathauto use.'),
    ),
    'field_article_issue_path' => array(
      'name' => t('Article Issue Path'),
      'description' => t('A version of field_news_issue suitable for pathauto use.'),
    ),
    'field_article_publication_path' => array(
      'name' => t('Article Publication Path'),
      'description' => t('A version of field_news_issue and field_publication_title suitable for pathauto use.'),
    ),
    'field_article_publication_title' => array(
      'name' => t('Article Publication Title'),
      'description' => t('A version of field_publication_title suitable for breadcrumb title use.'),
    ),
  );

  // Return them.
  return $info;
}

/**
 * Implements hook_tokens().
 */
function mysite_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();
  if ($type == 'node') {
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'field_issue_title':
          $values = array();
          if (!empty($data['node']->field_publication_title[LANGUAGE_NONE][0]['tid'])) {
            $term = taxonomy_term_load($data['node']->field_publication_title[LANGUAGE_NONE][0]['tid']);
            $values[] = $term->name;
          }
          else {
            $values[] = t('MISSING PUBLICATION');
          }
          if (!empty($data['node']->field_volume[LANGUAGE_NONE][0]['value'])) {
            $values[] = 'Volume ' . $data['node']->field_volume[LANGUAGE_NONE][0]['value'];
          }
          else {
            $values[] = t('MISSING VOLUME');
          }
          if (!empty($data['node']->field_issue[LANGUAGE_NONE][0]['value'])) {
            $values[] = 'Issue ' . $data['node']->field_issue[LANGUAGE_NONE][0]['value'];
          }
          else {
            $values[] = t('MISSING ISSUE');
          }
          if (!empty($data['node']->field_publication_date[LANGUAGE_NONE][0])) {
            $timestamp = reset($data['node']->field_publication_date[LANGUAGE_NONE]);
            $values[] = format_date($timestamp['value'], 'custom', 'F d, Y');
          }
          else {
            $values[] = t('MISSING DATE');
          }
          $replacements[$original] = implode(' - ', $values);
          break;

        case 'field_issue_path':
          if (!empty($data['node']->field_publication_date)) {
            $timestamp = reset($data['node']->field_publication_date[LANGUAGE_NONE]);
            $replacements[$original] = format_date($timestamp['value'], 'custom', 'Y/m-d');
          }
          break;

        case 'field_issue_publication_path':
          if (!empty($data['node']->field_publication_title)) {
            $replacements[$original] = 'taxonomy/term/' . $data['node']->field_publication_title[LANGUAGE_NONE][0]['tid'];
          }
          break;

        case 'field_article_issue_path':
          if (!empty($data['node']->field_news_issue)) {
            $nid = $data['node']->field_news_issue[LANGUAGE_NONE]['0']['target_id'];
            $issue = node_load($nid);
            $timestamp = reset($issue->field_publication_date[LANGUAGE_NONE]);
            $replacements[$original] = format_date($timestamp['value'], 'custom', 'Y/m-d');
          }
          break;

        case 'field_article_publication_path':
          if (!empty($data['node']->field_news_issue)) {
            $issue = node_load($data['node']->field_news_issue[LANGUAGE_NONE][0]['target_id']);
          }
          if (!empty($issue->field_publication_title)) {
            $replacements[$original] = 'taxonomy/term/' . $issue->field_publication_title[LANGUAGE_NONE][0]['tid'];
          }
          break;

        case 'field_article_publication_title':
          if (!empty($data['node']->field_news_issue)) {
            $issue = node_load($data['node']->field_news_issue[LANGUAGE_NONE][0]['target_id']);
          }
          if (!empty($issue->field_publication_title)) {
            $tid = $issue->field_publication_title[LANGUAGE_NONE][0]['tid'];
            $replacements[$original] = taxonomy_term_load($tid)->name;
          }
          break;
      }
    }
  }
  return $replacements;
}
